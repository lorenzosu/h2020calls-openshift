import os
import cPickle as pickle

import calendarparser
import html_generator
import daily_update

d = daily_update.H2020CallsDailyUpdate()
result = d.download_calendar()

cal = calendarparser.CallCalendar()
cal.parse_file(filename=os.path.join(os.environ['OPENSHIFT_REPO_DIR'],"calls.ics"))
out_file = os.path.join(os.environ['OPENSHIFT_DATA_DIR'],"cal_class.pkl")
with open(out_file, 'wb') as output:
    pickle.dump(cal, output, -1)

html_gen = html_generator.HTMLGenerator()
html_output = html_gen.to_html()

html_file = os.path.join(os.environ['OPENSHIFT_DATA_DIR'],"calls.html")
with open(html_file, 'w') as out_file:
    out_file.write(html_output)
print("\n----Daily update done ---")
#print(html_output)
