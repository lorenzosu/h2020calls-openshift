import os
import cPickle as pickle
from lxml import etree
from datetime import datetime
from collections import defaultdict

import calendarparser

class HTMLGenerator():
    def __init__(self):
        pickle_file = os.path.join(
                os.environ['OPENSHIFT_DATA_DIR'],
                "cal_class.pkl")
        with open(pickle_file, 'rb') as f:
            self.cal = pickle.load(f)

    def to_html(self):
        """ generate html """
        server_root_url = 'https://' + os.environ['OPENSHIFT_APP_DNS']
        html = """
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>H2020 Calls</title>
    <link rel="shortcut icon" href=""" + "'" + server_root_url + """/static/favicon.ico'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <style type="text/css">
    div.call_div {
        padding-top: 1em;
        }
    </style>
</head>
<body>
<div>
"""
        header_string = ("<h1>Next H2020 Calls from <u>" +
            self.cal.date_from.strftime("%d %b %Y") + "</u> to <u>" +
            self.cal.date_to.strftime("%d %b %Y") +
            "</u></h1></div>"
            )
        html += header_string
	html += "<div><p>As published in H2020 calendar available on: <a href='http://ec.europa.eu/research/participants/portal/desktop/en/opportunities/'>H2020 Participant Portal</a></p><p>Made by Lorenzo Sutton. Email: lorenzofsutton[removethis][at]gmail.com</p>"
        
        html += "<p> Generated:" + datetime.ctime(datetime.utcnow()) + " UTC </p></div>"
        div_id = 1
        for k in sorted(self.cal.date_dic.keys()):
            for e in self.cal.date_dic[k]:
                edic = e.event_dic
                call_string = (
                        "<div class='call_div'>\n\t<h2>" +
                        edic['SUMMARY'].replace("\\","") + 
                        "</h2>\n\t<p><em>Deadline Date: </em><strong>" +
                        edic['deadline_string'] +  "</strong>\n\t</p>\n" +
                        "\t<p>" + edic['DESCRIPTION'] + "</p>\n"
                    )
                
                html = html + call_string

                print "Fetching topics for call at: " + e.call_url
                topic_list = self.cal.call_page_parser(e.call_url)
                html += '<button type="button" class="btn btn-large btn-primary" data-toggle="collapse" data-target="#call_'+ str(div_id) +'" title="Click for call topics">Call Topics</button>' + "\n"
                html += "<div id='call_"+str(div_id) +"' class='collapse'>"
                html += "<ul class='topic_list'>"
                for t in topic_list:
                    line = "<li class='single_topic'>""<a href='"+ t[1].strip() + "'>" + t[0].encode('utf-8') + "</a></li>"
                    html += line
                div_id = div_id + 1
                html += "</ul></div>"
                html += "</div>\n"
        html += """     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        </body></html>"""
        return html

""" test 
cal = CallCalendar()
cal.parse_file()
ret_html = cal.to_html()
"""
#cal.printer()
#print ret_html

