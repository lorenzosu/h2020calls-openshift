import os
from flask import Flask, url_for

import html_generator

app = Flask(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True

@app.route('/')
def hello_world():
    return """
    <html><head><title>Welcome</title>
    <link rel="shortcut icon" href="./static/favicon.ico">
    </head>
    <body>
    <div><p>Hello. <a href="./h2020/calls">H2020 calls</a><p></div>
    </body>
    </html>
    """

@app.route('/h2020/calls')
def h2020():
    """Read the generated htmlfile in the data dir and return it."""
    #html_gen = html_generator.HTMLGenerator()
    html_file = os.path.join(os.environ['OPENSHIFT_DATA_DIR'],"calls.html")
    with open(html_file, 'r') as data_file:
        ret_html = data_file.read()
    return ret_html

if __name__ == '__main__':
    favicon_path = os.path.join(os.environ['OPENSHIFT_REPO_DIR'], 'favicon.ico')
    app.add_url_rule('/favicon.ico', redirect_to=url_for('static', filename=favicon_path))
    app.debug = False
    app.run()
