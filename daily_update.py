import os
import urllib

class H2020CallsDailyUpdate:
    def __init__(self):
        self.calendar_url = "http://ec.europa.eu/research/participants/portal/data/call/h2020/calls.ics"
        
    def download_calendar(self):
        output_file = os.path.join(os.environ['OPENSHIFT_REPO_DIR'],"calls.ics")
        result = urllib.urlretrieve(self.calendar_url, output_file)
        return result

h = H2020CallsDailyUpdate()
print("Downloading calendar...")
res = h.download_calendar()
print(res[1])
