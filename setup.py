from setuptools import setup

setup(name='H2020 Calls',
      version='0.1',
      description='H2020 Calls - Flask on OpenShift App',
      author='Lorenzo Sutton',
      author_email='lorenzosulorenzo@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask==0.10.1'],
     )
